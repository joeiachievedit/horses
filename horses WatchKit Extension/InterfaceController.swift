//
//  InterfaceController.swift
//  horses WatchKit Extension
//
//  Copyright (c) 2014 iAchieved.it LLC. All rights reserved.
//
import WatchKit
import Foundation

class InterfaceController: WKInterfaceController {
  
  @IBOutlet weak var horseImage: WKInterfaceImage!
  override init(context: AnyObject?) {
    super.init(context: context)
    
    horseImage.setImageNamed("frame-")
    horseImage.startAnimatingWithImagesInRange(NSMakeRange(0, 15),
      duration: 1,
      repeatCount: -1)
  }
  
  override func willActivate() {
    super.willActivate()
  }
  
  override func didDeactivate() {
    super.didDeactivate()
  }
  
  func sliderTapped(value: Float) {
    let duration:NSTimeInterval = -1*Double(value) + 1.5
    horseImage.startAnimatingWithImagesInRange(NSMakeRange(0, 15),
      duration: duration,
      repeatCount: -1)
  }
  
}
